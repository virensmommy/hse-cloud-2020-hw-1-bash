#!/bin/bash

awk '{ total=$2+$3+$4;
avg = total/3;
if (avg >= 80) ans = "A";
else if (avg >= 60 && avg < 80) ans = "B";
else if (avg >= 50 && avg < 60) ans = "C";
else ans = "FAIL";
print $0, ":", ans; }'
