#!/bin/bash

read word;
i=0;

while [[ ! -z "$word" ]]
do
	if [[ ${word[0]} =~ [A-Z] ]]
	then
		word="."${word:1}
	fi
	array[i]=$word;
	i=$(( $i + 1 ));
	read word;
done

n=${#array[@]}

for (( i=0; i<n-1; ++i ));
do
	echo -n "${array[i]} ";
done

echo "${array[n-1]}";
