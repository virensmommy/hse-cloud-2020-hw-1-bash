#!/bin/bash

read word;
i=0;

while [[ ! -z "$word" ]]
do
    array[i]=$word;
    i=$(( $i + 1 ));
    read word;
done

n=${#array[@]}

for (( i=0; i<3; ++i ));
do
    for (( j=0; j<n; ++j ));
    do
        array[$i * $n + $j]=${array[$j]};
    done
done

for (( i=0; i<n*3-1; ++i ));
do
    echo -n "${array[i]} ";
done

echo "${array[n*3-1]}";
