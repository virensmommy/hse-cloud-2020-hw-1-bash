#!/bin/bash

read word;
i=0;

while [[ ! -z "$word" ]]
do
	if [[ !(( $word =~ "a" || $word =~ "A" )) ]]
	then
		array[i]=$word;
		i=$(( $i + 1 ));
	fi
	read word;
done

n=${#array[@]}

for (( i=0; i<n-1; ++i ));
do
	echo -n "${array[i]} ";
done

echo "${array[n-1]}";
